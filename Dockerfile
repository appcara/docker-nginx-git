FROM nginx:1.13.9-alpine

RUN apk add --update git && rm -rf /usr/share/nginx/html

EXPOSE 80

STOPSIGNAL SIGTERM

# docker run --rm -e GIT_REPO=https://bitbucket.org/appcara/devops-demo-website.git nginx-git
CMD [[ "$GIT_REPO" == "" ]] && echo "GIT_REPO is expected but missing" || (git clone $GIT_REPO /usr/share/nginx/html && nginx -g "daemon off;")
